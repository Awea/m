# m

An experimental make wrapper

## Requirements

Depends on GNU Make.

On macOS you'll need to update to the last version:

``` sh
brew install make
```

## Features

- Allow running a makefile from a parent's folder
- Define one passthrough command with some hook support
- Self-wrap a makefile, for users not using m globally

### Running a Makefile from a parent's folder

This allows you to run a Makefile in a parent folder from a subfolder.

Let say you have the following:

``` sh
$ ls
Makefile src
$ ls src/a.c
```

If you go to `src` and run `make`, you'll get an error:

``` sh
$ make
make: *** No targets specified and no makefile found.  Stop.
```

`m` is instead going to look in the parent folder if there is no Makefile in the
current folder. Making it easy to run the build from wherever in the project.

## Define a passthrough command

This allows you to declare a command that will be executed instead of passed to
the underlying Makefile.

Example:
```Makefile
.PASSTHROUGHT: env
```

If one run `m env`, it will simply run the `env` command (and all its
possible argument).

This commit also had a hook target: `m-passthrough-pre-hook` which allow
you to execute something just before the passthrough run. This could be
used to prepare the environment before a command is run.

```Makefile
m-passthrough-pre-hook:
        source env.sh || true
```

Will run `source env.sh || true` before running the passthrough command.

##  Self-wrap a makefile

If you embed the version of `m` you are using in your project, you can make it self-wrap your Makefile so that users don't using `m` will anyway use its functionality (albeit with fewer colours in the shell).

This allows you to embed `m` in your project, and prevent user of your project
from having to install a specific niche tool just for your project.

To do so, add `m` in your repo, and add the following at the very top of your
Makefile (adapt the `MPATH` variable if needed):

``` makefile
# This is some *magic* to switch to m instead of continuing the current
# Makefile.
#
# So even if someone launch the makefile with gmake, it will be executed
# via the repo's local m version.
MPATH = './m'
# First, we check that we are using a version that has the file command
ifeq (4.0,$(firstword $(sort $(MAKE_VERSION) 4.0)))
  # Then, we ensure that we are not already wrapped by m
  ifneq ($(MWRAPPED),1)
    # Now, we execute m with the arguments passed to make, and we safe the
    # output in a file. This is because the $(shell ) function does not
    # correctly returned a multiline variable, but everything on one line.
    exec := $(shell $(MPATH) $(MAKECMDGOALS) > m.out)
    # We read the output, and write it with $(info)
    log := $(file < m.out)
    $(info $(log))
    # Now, this is where the real trick is, now we have already executed
    # what has been passed to this Makefile.
    #
    # To prevent it from re-executing the targets, we replace the content
    # of the args passed to the makefile by nothing. Now the Makefile
    # don't have anything else to execute, and thus, just exit.
    $(eval $(MAKECMDGOALS):;@:)
  endif
endif
```

Now, if one use `make` to execute the makefile, `m` is going to be executed
instead and will ensure the original `make` won't have anything to do.
