.DEFAULT_GOAL := help

# ###########################################################################
# This is some *magic* to switch to m instead of continuing the current
# Makefile.
#
# So even if someone launch the makefile with gmake, it will be executed
# via the repo's local m version.

MPATH = './m'

# First, we check that we are using a version that has the file command
# Do not change the space in tabs, otherwise make will fail to parse
ifneq (4.0,$(firstword $(sort $(MAKE_VERSION) 4.0)))
  $(error "Error: GNUMake 4+ is required")
endif

# Then, we ensure that we are not already wrapped by m
ifneq ($(MWRAPPED),1)
  # Now, we execute m with the arguments passed to make, and we save the
  # output in a file. This is because the $(shell ) function does not
  # correctly returned a multiline variable, but everything on one line.
  exec := $(shell $(MPATH) $(MAKECMDGOALS) > m.out)
  #exit = $(call execute_m, $(MAKECMDGOALS))
  # TODO: make it possible to check the error code of m. So one can error
  # out here.
  # See: https://www.gnu.org/software/make/manual/html_node/Shell-Function.html

  # We read the output, and write it with $(info)
  log := $(file < m.out)
  $(info $(log))

  # Now we are back in the Makefile, and to avoid re-running the target, we
  # use one last trick that you can find at the end of the Makefile.
endif
# ###########################################################################

.PASSTHROUGH: ls

m-passthrough-pre-hook:
	echo $(MWRAPPED)
	echo test
	@source env.sh || true
  
help:
	@echo "Good luke with that"
	@ls
	@echo "Wait..."
	@echo '--just-print print the **content** of the recipies!'
	@echo "that's pretty cool..."

# Now, that's were the real tric happens, at that point we have already
# executed via m what this Makefile should be executing now.
# 
# To prevent the target to run again we rewrite all the target with do-nothing
# target (@:).  This will re-declare existing rules with as do-nothing, and
# will thus redeclare them.  And since it is the very last rule, it is the
# override that win over any previous one.
ifneq ($(MWRAPPED),1)
  .DEFAULT_GOAL := ""
  $(eval $(MAKECMDGOALS):;@:)
endif